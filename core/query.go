package core

import (
	"apiconfig/tools"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"reflect"
	"strings"
	"unsafe"
)

type Input struct {
	Api       string                 `json:"api"`
	Wheres    map[string]interface{} `json:"wheres"`
	Page      int                    `json:"page"`
	Rows      int                    `json:"rows"`
	TableName string                 `json:"config"`
	Custom    string                 `json:"custom"`
}

var db *gorm.DB

var totalchan chan int64 = make(chan int64) //通道

func OrmQuery(c *ApiConfig, input Input) (bool, []map[string]interface{}, int64) {
	res, conn := tools.ReadConfig()
	if !res {
		return false, nil, 0
	}
	db, err := gorm.Open("mysql", conn)
	if err != nil {
		fmt.Errorf("创建数据库连接失败:%v", err)

	}
	defer db.Close()
	db.SingularTable(true)
	db.LogMode(true)
	db.SetLogger(tools.Logger())
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	//解析json的wheres
	a := WhereQuery{}
	b := WhereQuery{}

	var wq *WhereQuery
	if c.Wheres != nil {
		wq = a.GenerateWhere(c.Wheres)
		//拼where条件
		for _, v := range wq.QueryParams {
			newValue, _ := json.Marshal(v)
			key := string(newValue)
			wq.QuerySql = strings.Replace(wq.QuerySql, "?", key, 1)
		}
	} else {
		wq = &WhereQuery{}
		wq.QuerySql = "1=1"
	}

	var wq2 *WhereQuery
	if input.Wheres != nil {
		wq2 = b.GenerateWhere(input.Wheres)
		//拼where条件
		key := ""
		for _, v := range wq2.QueryParams {
			switch v.(type) {
			case string:
				key = v.(string)
			default:
				newValue, _ := json.Marshal(v)
				key = string(newValue)
			}
			wq2.QuerySql = strings.Replace(wq2.QuerySql, "?", key, 1)
		}
	} else {
		wq2 = &WhereQuery{}
		wq2.QuerySql = "1=1"
	}

	//异步计算总记录数
	resultChan := make(chan int64)
	go func() {
		var total int64
		totalRow, err := db.Raw(fmt.Sprintf("select count(1) from %s where 1=1 and %s and %s", input.TableName, wq.QuerySql, wq2.QuerySql)).Rows()
		if err != nil {
			return
		}
		for totalRow.Next() {
			err := totalRow.Scan(
				&total,
			)
			if err != nil {
				fmt.Println("GetKnowledgePointListTotal error", err)
				continue
			}
		}
		totalRow.Close()
		resultChan <- total
	}()

	pagination := "1"
	//组装分页
	if input.Page > 0 && input.Rows > 0 {
		pagination = fmt.Sprintf("%d,%d", input.Page-1, input.Rows)
	}

	sql := fmt.Sprintf("select %s from %s where 1=1 and %s and %s limit %s", c.Field, input.TableName, wq.QuerySql, wq2.QuerySql, pagination)

	rows, err := db.Table(input.TableName).Raw(sql).Rows()
	if err != nil {
		log.Println(err.Error())
		return false, nil, 0
	}
	mData := scanRows2map(rows)
	return true, mData, <-resultChan

}

func CustomOrmQuery(c *CustomApiConfig, input Input) (bool, []map[string]interface{}, int64, string) {
	res, conn := tools.ReadConfig()
	if !res {
		return false, nil, 0, "数据库配置错误"
	}
	db, err := gorm.Open("mysql", conn)
	if err != nil {
		fmt.Errorf("创建数据库连接失败:%v", err)

	}
	defer db.Close()
	db.SingularTable(true)
	db.LogMode(true)
	db.SetLogger(tools.Logger())
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	querysql := ""
	if c.Field != "" {
		querysql = fmt.Sprintf(c.Sql, c.Field)
	}

	if input.Custom != "" {
		custom_arr := strings.Split(input.Custom, ",")
		for _, v := range custom_arr {
			querysql = strings.Replace(querysql, "?", "'"+v+"'", 1)
			c.Sql = strings.Replace(c.Sql, "?", "'"+v+"'", 1)
		}
	}

	//异步计算总记录数
	resultChan2 := make(chan int64)
	go func() {
		var total int64
		asyncsql := strings.Replace(c.Sql, "%s", "count(1)", 1)
		totalRow, err := db.Raw(asyncsql).Rows()
		if err != nil {
			return
		}
		for totalRow.Next() {
			err := totalRow.Scan(
				&total,
			)
			if err != nil {
				fmt.Println("GetKnowledgePointListTotal error", err)
				continue
			}
		}
		totalRow.Close()
		resultChan2 <- total
	}()

	if input.Page < 1 {
		input.Page = 1
	}
	if input.Rows == 0 {
		input.Rows = 1
	}

	sql := querysql + fmt.Sprintf(" limit %d,%d", input.Page-1, input.Rows)

	rows, err := db.Table(input.TableName).Raw(sql).Rows()
	if err != nil {
		log.Println(err.Error())
		return false, nil, 0, "查询失败"
	}
	mData := scanRows2map(rows)
	return true, mData, <-resultChan2, "查询成功"

}

func scanRows2map(rows *sql.Rows) []map[string]interface{} {
	res := make([]map[string]interface{}, 0)          //  定义结果 map
	colTypes, _ := rows.ColumnTypes()                 // 列信息
	var rowParam = make([]interface{}, len(colTypes)) // 传入到 rows.Scan 的参数 数组
	var rowValue = make([]interface{}, len(colTypes)) // 接收数据一行列的数组

	for i, colType := range colTypes {
		rowValue[i] = reflect.New(colType.ScanType())           // 跟据数据库参数类型，创建默认值 和类型
		rowParam[i] = reflect.ValueOf(&rowValue[i]).Interface() // 跟据接收的数据的类型反射出值的地址

	}
	// 遍历
	for rows.Next() {
		rows.Scan(rowParam...) // 赋值到 rowValue 中
		record := make(map[string]interface{})
		for i, colType := range colTypes {

			if rowValue[i] == nil {
				record[colType.Name()] = ""
			} else {
				record[colType.Name()] = Byte2Str(rowValue[i].([]byte))
			}
		}
		res = append(res, record)
	}
	return res
}

// []byte to string
func Byte2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
