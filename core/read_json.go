package core

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type ApiConfig struct {
	Api    string                 `json:"api"`
	Field  string                 `json:"field"`
	Wheres map[string]interface{} `json:"wheres"`
}

type CustomApiConfig struct {
	Api   string `json:"api"`
	Sql   string `json:"sql"`
	Field string `json:"field"`
}

func FindFile(filename, apiname string) *ApiConfig {
	jsonFile, err := os.Open(fmt.Sprintf("./files/%s.json", filename))
	if err != nil {
		fmt.Println("error opening json file")
		return nil
	}
	defer jsonFile.Close()

	jsonData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println("error reading json file")
		return nil
	}

	var api []ApiConfig
	json.Unmarshal(jsonData, &api)
	for _, v := range api {
		if v.Api == apiname {
			return &v
		}
	}
	return nil
}

func CustomFindFile(filename, apiname string) *CustomApiConfig {
	jsonFile, err := os.Open(fmt.Sprintf("./files/%s.json", filename))
	if err != nil {
		fmt.Println("error opening json file")
		return nil
	}
	defer jsonFile.Close()

	jsonData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println("error reading json file")
		return nil
	}

	var api []CustomApiConfig
	json.Unmarshal(jsonData, &api)
	for _, v := range api {
		if v.Api == apiname {
			return &v
		}
	}
	return nil
}
