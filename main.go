package main

import (
	"apiconfig/core"
	"github.com/gin-gonic/gin"
)

func main() {
	//Default返回一个默认的路由引擎
	r := gin.Default()
	v1 := r.Group("/api")
	{
		v1.GET("/query/test", func(c *gin.Context) {
			c.JSON(200, gin.H{"code": 200, "message": "请求格式错误", "data": ""})
		})
		v1.POST("/query/:file", func(c *gin.Context) {
			file := c.Param("file")
			var input core.Input
			err := c.ShouldBind(&input)
			if err != nil {
				c.JSON(200, gin.H{"code": -1, "message": "请求格式错误", "data": ""})
				return
			}

			if string(file[0]) == "_" {
				v := core.CustomFindFile(file, input.Api) //读取api配置
				if v == nil {
					c.JSON(200, gin.H{"code": -1, "message": "获取配置失败", "data": ""})
					return
				}
				res, data, count, msg := core.CustomOrmQuery(v, input) //查询
				if !res {
					c.JSON(200, gin.H{"code": -1, "message": msg, "data": "", "count": count})
					return
				}
				c.JSON(200, gin.H{"code": 200, "message": msg, "data": data, "count": count})
			} else {
				v := core.FindFile(file, input.Api) //读取api配置
				if v == nil {
					c.JSON(200, gin.H{"code": -1, "message": "获取配置失败", "data": ""})
					return
				}
				res, data, count := core.OrmQuery(v, input) //查询
				if !res {
					c.JSON(200, gin.H{"code": -1, "message": "pong", "data": "", "count": count})
					return
				}
				c.JSON(200, gin.H{"code": 200, "message": "成功", "data": data, "count": count})
			}

		})

	}

	r.Run(":9989") // listen and serve on 0.0.0.0:8080
}
