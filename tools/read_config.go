package tools

import (
	"fmt"
	"github.com/spf13/viper"
)

//读取配置
func ReadConfig() (bool, string) {
	config := viper.New()
	config.AddConfigPath("./files/")
	config.SetConfigName("db")
	config.SetConfigType("json")

	if err := config.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Println("找不到配置文件..")
			return false, ""
		} else {
			fmt.Println("配置文件出错..")
			return false, ""

		}
	}
	host := config.GetString("host.origin")
	port := config.GetString("host.port")
	username := config.GetString("username")
	password := config.GetString("password")
	db := config.GetString("db")

	sql := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", username, password, host, port, db)
	return true, sql
}
