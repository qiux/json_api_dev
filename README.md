### 一、描述
普通查询接口占到整体接口开发量的80%左右，本服务目的在于优化普通接口的开发量。
包括根据条件查询详情、根据条件查询列表，分页查询。
使用到的技术：gin+gorm，边学边写。
### 二、JSON配置实现通用查询
#### 1、JSON模板
##### 字段说明
| 字段 | 说明 | 例子 |
| --- | --- | --- |
| api | 作为接口的描述和id | getList |
| field | 需要查询的字段 | id,real_name |
| wheres | 查询条件（有专门的wheres语法说明） |  { "id": 13, "created_at": "not null"} |
| member.json | 配置文件名通过"_"区分是定制化sql，还是通用查询。 | member.json;_member.json |
| config | 表名 |  |

```json
[
  {
    "api" : "getList",
    "field" : "id,real_name",
    "wheres":
    {
      "created_at": "not null"
    }
  },
    {
      "api" : "getUserById",
      "field" : "id,real_name",
      "wheres":
      {
        "id": 13,
        "created_at": "not null"
      }
    },
  {
    "api" : "getUser",
    "field" : "id,real_name",
  }
]
```
#### 2、接口模板
请求URL格式：[http://localhost:9981/api/query/](http://localhost:9981/api/query/tb_members){{配置文件名}}
请求方法：POST

| 字段 | 说明 | 例子 |
| --- | --- | --- |
| api | 对应配置文件标识 |  |
| page | 页码 |  |
| rows | 每页记录数 |  |
| wheres | 查询条件 |  |

##### 例子
POST:http://localhost:9981/api/query/member
```json
{
    "api":"getList",
    "page":1,
    "rows":2,
    "wheres":    
    {
      "id": 3
    }
}
```
##### 返回
```json
{
	"code": 0,
	"count": 1181,
	"data": [
		{
			"id": "1",
			"real_name": "137****4846"
		},
		{
			"id": "2",
			"real_name": "137****6997"
		}
	],
	"message": "成功"
}
```
| 字段 | 说明 | 例子 |
| --- | --- | --- |
| code | 返回状态 | 0 成功，-1 失败 |
| count | 总记录数 |  |
| data | 查询的结果 |  |
| message | 查询结果 |  |

#### 3、wheres条件

- 等于=

"age":8  // sql: age = 8

- 大于>

 	"age": [">", "12"] // age > 12

- like

username": ["like", "admin"] // username like '%admin%'

- not null

"created_at": "not null" // created_at is not null

- in

"email":    ["in", "'admin','dd','gg','ee'"],//email in ('admin','dd','gg','ee')

```json
    "wheres":
    {
      "username": ["like", "admin%|editor"],
      "email":    ["in", "'admin','dd','gg','ee'"],
      "or":
      {
        "username": ["like", "admin"],
        "age":8
      },
      "age": 6,
      "updated_at": "null",
      "created_at": "not null"
    }
```
### 三、json配置实现定制化查询（多表关联）
#### 

#### 1、json配置
| 字段 | 说明 | 例子 |
| --- | --- | --- |
| api | 作为接口的描述和id | getList |
| field | 需要查询的字段 | a.user_name,b.order_no,a.id |
| sql | 定制化sql语句 |   |
| _member.json | 配置文件名 "_"开头定制化配置 | member.json；_member.json |

```json
[
  {
    "api" : "getList",
    "field" : "a.user_name,b.order_no,a.id",
    "sql":"select %s from tb_members a left join tb_order b on a.id = b.user_id where a.id = ? or a.id = ?"
  }
]
```
#### 2、请求模板
| 字段 | 说明 | 例子 |
| --- | --- | --- |
| api | 对应配置文件标识 |  |
| page | 页码 |  |
| rows | 每页记录数 |  |
| custom | 对应sql里面的？，按顺序填充sql | where id = ? and name =?,custom:"12,'baba'"->where id = 12 and name = 'baba' |

##### 例子
POST:http://localhost:9981/api/query/_member
```json
{
    "api":"getList",
    "page":1,
    "rows":10,
    "custom":"13,12" ->select a.user_name,b.order_no,a.id from tb_members a left join tb_order b on a.id = b.user_id where a.id = 13 or a.id = 12
}
```
##### 返回
```json
{
	"code": 0,
	"count": 76,
	"data": [
		{
			"id": "12",
			"order_no": "123432424344",
			"user_name": "15200000000"
		},
		{
			"id": "12",
			"order_no": "1231231231123",
			"user_name": "15200000000"
		}
	],
	"message": "成功"
}
```

### 四、开发计划
1、能够自定义化查询（已实现）
2、加入关注、点赞、收藏等通用接口
3、加入提交接口
